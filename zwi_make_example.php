<?php

/**
* A simple example showing how to make a ZWI file inside a PHP code.
*
* S.Chekanov
*
*/

// Here define your input for the ZWI file example.zwi
$zipfilename="example.zwi";
$title="Title"; // title of the article
$publisher="enhub";
$time_created=1651262556;
$time_modified=1651262556;
$full_url="https://enhub.org";
$Lang="en";
$comment="";
$license="GPL 3.0";
$description="Short description of the article (plain text)";
$author="";
// actual content
$articleHTML="<H1>Article</H2> This is my <i>example article</i> in HTML form";
$articleTXT="Article\n This is my example article in plain text";


// do not change below
// -------------------
if (file_exists($zipfilename)==true) unlink($zipfilename);
$zip = new ZipArchive();
if ($zip->open($zipfilename, ZipArchive::CREATE)!==TRUE) {
	exit();                         }


$content_array= array();
$content_array["article.html"]=sha1($articleHTML);
$content_array["article.txt"]=sha1($articleTXT);


$tt['ZWIversion'] = 1.3;
$tt['Primary'] = "article.html";
$tt['Title'] =$title;
$tt['Content'] = $content_array;
$tt['Publisher']=$publisher;
$tt['TimeCreated'] = $time_created;
$tt['LastModified'] = $time_modified;
$tt['GeneratorName'] = "Wordpress";
$tt['SourceURL'] = $full_url;
$tt['Lang'] = $Lang;
$tt['Comment'] = $comment;
$tt['Rating'] = "";
$tt['License'] = $license;
$tt['Description']=$description;
$tt['Author']=$author;

// add metadata
$zip->addFromString("metadata.json", json_encode($tt,JSON_PRETTY_PRINT));

$zip->addFromString("article.html", $articleHTML);
$zip->addFromString("article.txt", $articleTXT);

// if this article needs some style files, add them!
//$zip->addFile($path.'/css/common.css',  "data/css/common.css");


// if this article has images, add image files
//$in_img="data/media/images/".$img;
//$zip->addFile($mage_path,  $in_img);

// if you cave CSS and images, add json that describe them
// $zip->addFromString("media.json", json_encode($zwiImages,JSON_PRETTY_PRINT));
// look at the example: https://gitlab.com/ks_found/ZWIMaker/-/blob/main/Mzwi.hooks.php

// close the file
$zip->close();

