#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This program is used to update meta description of ZWI files.
#
# @version 1.0, June 8, 2022 
# S.V.Chekanov, L.Sanger, C.Gribneau  
# Organzation: KSF
# 
import re, os, json
import argparse, subprocess

from zipfile import ZipFile
from short_description import *

re_description = re.compile(
    r'(?<=[.;?!])\s'
)

def zwiUpdate(zfile):
    """ Main function to update ZWI file"""

    unzipped_file = ZipFile(zfile, "r")
    metadata = unzipped_file.read("metadata.json")
    media = unzipped_file.read("media.json")
    articlehtml = unzipped_file.read("article.html")
    wikitext = unzipped_file.read("article.wikitext")
    articletxt = unzipped_file.read("article.txt")

    articlehtml = articlehtml.decode('utf-8')
    wikitext  =   wikitext.decode('utf-8')
    articletxt  =  articletxt.decode('utf-8')

    #print(metadata)
    js_metadata = json.loads(metadata)

    atitle = js_metadata["Title"]
    asource = js_metadata["Publisher"]

    ## find correct topics
    topics = []

    # topics are set for HandWiki only
    if asource == "handwiki":
        tt = atitle.split(":",1)
        if len(tt) > 1:
            topics.append(tt[0])
            atitle = tt[1]

    shortdesc = getWikiShortDescription(
        asource, 
        wikitext
    )

    if len(shortdesc) > 3:
        topics.append(shortdesc)

    # update topics
    js_metadata["Topics"] = topics

    ############### title #################################
    ftitle = atitle.replace("_"," ")
    js_metadata["Title"] = ftitle 

    ##############################  Description ####################
    # short description as implemented by Larry
    with open("/tmp/tmp.html","w") as xff:
        xff.write(articlehtml)

    cmd = f"ruby {script_dir}/tools/desc_getter/desc_getter.rb /tmp/tmp.html {asource}"

    p = subprocess.Popen(
        cmd, 
        stdout=subprocess.PIPE, 
        shell=True
    )

    output, err = p.communicate()
    p_status = p.wait()
    description=output.decode()

    # Get 2 sentances
    if description:
        if len(description) > 10:
            description = ' '.join(re_description.split(description)[:2])

            description = description.replace("\n","").strip()

            if description.endswith("."):
                description = description[0:len(description)-1] # no full stop

            if description.endswith(":"): 
                description = description[0:len(description)-1] # no full stop

    if description:
        if len(description)>9:
            print (f" :: Ruby short description :: {description}")

    # Fall back
    if not description:
        description = getShortDescription(
            asource,
            wikitext,
            articlehtml,
            articletxt
        )

    if len(description) < 10:
        description = getShortDescription(asource,wikitext,articlehtml,articletxt)
        print (f" :: Python short description :: {description}")

    unzipped_file.close()

    # Directly from dictionary
    with open('metadata.json', 'w') as outfile:
        json.dump(
            js_metadata, 
            outfile,
            indent=4
        )

    print("Update metadata.json in  ZWI file")
    os.system(f"zip -u \"{zfile}\" metadata.json")
    os.remove("metadata.json")

#### input parameters ####
kwargs = {}
parser = argparse.ArgumentParser()
parser.add_argument("-q", "--quiet", action="store_true", help="don't show verbose")
parser.add_argument("-z", "--zwi", help="Input ZWI file. Output will be the same as input")

args = parser.parse_args()
args.verbose = not args.quiet

script_dir = os.path.dirname(os.path.realpath(__file__))
print(f"From= {script_dir}")
print(f" : Input ZWIfile= {args.zwi}")
print(f" : Is verbose= {args.verbose}")
   
zwiUpdate(args.zwi)