#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Create 2-level index files for the ZWI database. All times are in the UTC.  
# To make all index files for all publishers by rescanning ZWI file use: 
#        python3 make_index.py -i <path> where <path> points to the directory with ZWI files.
# To update the index of specific publishers and the global index file use:
#        python3 make_index.py -i <path> -p "publisher1,publisher2" 
# Use the comma-seprated list of publishers. Note scanning of directories for other publishers will not be performed
#
# This directory should ends as ZWI/en (without / at the end).
# This module can also update the index of a specific publisher  
# 
# S.V.Chekanov, H.Sanger (KSF)
# Version 1.0, Nov 10, 2022
# Version 1.1, Nov 13, 2022
#

import time
import argparse
import sys, io, os
import glob, csv, gzip
import requests

from os.path import exists
from datetime import datetime

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

# create index file for the ZWI directory
# Optionally, specify list with excluded encyclopedias 
def createIndexFiles(
    basedirZWI: str,
    excluded: list = [], 
    verbose: bool = True
    ) -> dict:
    """ Create new index files from the directory basedirZWI, but excluding excluded[] 
        basedirZWI: str
                base directry with ZWI
        excluded: list
                list of exluded publishes
        return: dict{}
                dictinary with the index 
    """

    if (not basedirZWI.endswith("/en") and not basedirZWI.endswith("/en/")):
        basedirZWI += "/en"

    start = time.time()
    outfile = f"{basedirZWI}/index.csv.gz"
    data = []

    nn = 0
    if verbose:
        print("Initial scan of directories")

    for f in glob.glob(f"{basedirZWI}/*/*", recursive=False):

        remove=False

        for ex in excluded:
            if f.find(ex) > -1:
                remove = True

        if remove:
            continue

        if os.path.isdir(f):
            name = f.replace(
                basedirZWI,
                ""
            ).strip()

            if name.startswith("/"):
                name = name[1:len(name)]

            if name.endswith("/"):
                name = name[0:len(name)-1]

            if len(name) < 2:
                continue 

            xtime = os.path.getmtime(f) # modification time 
            data.append( [int(xtime), name, f] )
            nn += 1

    # sort according to time
    data.sort(
        key = lambda x: x[0],
        reverse = True
    )

    mainIndex = {}
    dirnumber = {}
    dirsizes = {}

    if verbose: 
        print(f"Making local index files for {nn} encyclopedias")

    nn = 0
    xsum = 0

    for d in data:
        m_name = d[1].strip() # short name of the directory, i.e. wikitia/wikitia.com 
        xdir = d[2] # full path, i.e ZWI/en/wikitia/wikitia.com 
        localindex= f"{xdir}.csv.gz"

        #print("Creating:"+localindex)
        output = gzip.open(localindex, 'w')

        #slow
        #flist=glob.glob(xdir+"/*.zwi", recursive=False)
        #sorted_by_mtime_descending = sorted(flist, key=lambda t: -os.stat(t).st_mtime)

        flist = os.scandir(f"{xdir}/")
        articles = []
        dir_bytes = dir_number = 0

        for item in flist: 
            shortname = item.name
            if shortname.endswith(".zwi"):

                shortname = shortname[0:len(shortname)-4]
                #path=item.path
                xsize = int(item.stat().st_size) 
                xtime = int(item.stat().st_mtime) # time of last modification. 

                #xtime=int(os.path.getmtime(f))
                #xsize=int(os.path.getsize(f))
                #shortname=Path(f).stem
                #fulllist.append(str(xtime)+"|"+str(xsize)+"|"+shortname)

                articles.append([xtime, xsize, shortname])

                nn += 1

                xsum += xsize
                dir_bytes += xsize
                dir_number += 1

        #print("Create dictionary=",m_name)
        dirnumber[m_name] = dir_number
        dirsizes[m_name] = int(dir_bytes) # evaluate correct size of the directory
        articles.sort(
            key = lambda x: x[0],
            reverse=True
        )

        if verbose:
            print(f"Creating: {localindex} with {len(articles)} articles")

        for entry in articles:
            line = f"{entry[0]}|{entry[1]}|{entry[2]}\n"
            output.write(line.encode('utf-8')) 

        output.close()
        os.system(f"chmod 777 {localindex}")
        mainIndex[m_name] = articles
        #print(m_name,len(articles) )

    if verbose:
        print("")
        print(f"Total files= {nn} Total size= {xsum/1073741824.0} GB")

    globalIndex={}
    nn = totsize = 0
    if verbose:
        print(f"Write new global index: {outfile}")

    output = gzip.open(outfile, 'wb')

    for d in data:
        xtime = str(d[0])
        xname = str(d[1])
        xnumber = dirnumber[xname]  # number of files 
        xsize = dirsizes[xname] # size of this directory

        # clean-up
        xname = xname.replace(
            basedirZWI,
            ""
        )

        if xname.startswith("/"):
            xname = xname[1:len(xname)]

        if xname.endswith("/"):
            xname = xname[0:len(xname)-1]

        totsize += xsize 
        line = f"{xtime}|{xsize}|{xnumber}|{xname}\n"
        articles = mainIndex[xname]

        globalIndex[xname] = [xtime, xsize, xnumber, articles]
        output.write(line.encode('utf-8'))
        nn += 1

    output.close()
    os.system(f"chmod 777 {outfile}")

    if verbose:
        print(f"Total encyclopedias= {nn} Total size= {totsize/1073741824.0} GB")

    end = time.time()
    if verbose:
        print(f"Used time= {end - start}")

    return globalIndex

##############################################
### short summary of index
##############################################
def shortSummaryOfIndex(
    globalIndex: dict
    ) -> list[int]:
    """ returns a short summary of the global index file 
        [publishers, size, n_articles]
    """

    ntot = nsize = npub = 0
    for key, data in globalIndex.items():

        ntot += int(data[2]) 
        nsize += int(data[1])

        npub += 1

    return [npub, nsize, ntot]

###################################################
##################  writeIndexFiles ###############
def writeIndexFiles(
    basedirZWI: str,
    globalIndex: dict,
    publist: list = [],
    verbose: bool = True
    ):
    """ Write new index files to the disk for the list of  publishers (or all by default)"""

    outfile = f"{basedirZWI}/index.csv.gz"

    nn = 0
    totsize = 0

    if verbose:
        print(f"Update global index: {outfile}")

    output = gzip.open(outfile, 'wb')

    for key, val in globalIndex.items():

        xtime = str(val[0])
        xsize = str(val[1])
        xnumber = str(val[2])
        totsize += int(xsize) 

        line = f"{xtime}|{xsize}|{xnumber}{key}\n"

        output.write(line.encode('utf-8'))
        nn += 1

    output.close()
    os.system(f"chmod 777 {outfile}")

    if verbose: 
        print(f"Total encyclopedias= {nn} Total size= {totsize/1073741824.0} GB")
        print("Update publisher's index files")

    for key, val in globalIndex.items():
        keys = key.split("/")

        consider = False

        # could be done in a single line
        # but readability is much nicer
        if keys[0] in publist:
            consider = True

        if not consider:
            continue 
        
        localindex= f"{basedirZWI}/{key}.csv.gz"

        if verbose:
            print(f"  Creating: {localindex}")

        output = gzip.open(localindex, 'w')

        filllist = val[3]
        if verbose:
            print(f"  Write= {len(filllist)} entries from publisher {key}")

        for en in filllist:
            txt = f"{en[0]}|{en[1]}|{en[2]}\n"
            output.write(txt.encode('utf-8'))

        output.close()
        os.system(f"chmod 777 {localindex}")

######################################################
# Update index files for the publisher
#####################################################
def updateIndexFiles(
    basedirZWI: str,
    publist: list = [],
    verbose: bool = True
    ):
    """ Update index files for the list of specific publisher. No scanning of other publishers """

    globalIndex = getIndex(basedirZWI) # get global index first
    if not globalIndex: # error occurred
        return

    #print("Update index:",basedirZWI,"for publishers:",publist)

    pubkeys = []
    pubdata = [] 

    # fetch the publisher
    for key, val in globalIndex.items(): 
        keys = key.split("/")

        if keys[0] in publist:
            pubdata.append(val) 
            pubkeys.append(key) # name of the publisher/directory 

    if len(pubdata) == 0: 
        print(f"The publishers: {publist} are not found")
        return False 

    if verbose:
        print(f"Following publishers to be updated: {', '.join(pubkeys)}")

    xsum = 0

    # now scan all the  files for this list of publishers
    for xdir in pubkeys:
        flist = os.scandir(f"{basedirZWI}/{xdir}/")

        dir_bytes = 0
        dir_number = 0
        fulllist = []

        for item in flist:
            shortname = item.name
            if shortname.endswith(".zwi"):
                shortname = shortname[0:len(shortname)-4]
                xsize = int(item.stat().st_size) # file size
                xtime = int(item.stat().st_mtime) # time of last modification.

                fulllist.append([xtime, xsize, shortname])
                xsum += xsize
                dir_bytes += xsize
                dir_number += 1

        # sort according to date
        fulllist.sort(
            key = lambda x: x[0],
            reverse=True
        )

        # most recent time:
        recent = fulllist[0][0]
        globalIndex[xdir] = [
            recent,
            dir_bytes,
            dir_number,
            fulllist
        ]

        if verbose:
            print(f"  - Files updated: {len(fulllist)} Publisher: {xdir} Updated on: {datetime.fromtimestamp(recent)}")

    # write new index files
    writeIndexFiles(
        basedirZWI,
        globalIndex,
        publist
    )

    return


# Read global index files for all publishers from local directory. 
# The structure is: dic{key}=list
def getIndex(
    basedirZWI: str,
    verbose: bool = True
    ) -> dict | None:
    """ Return global index of all publishers using local directory """

    outfile = f"{basedirZWI.rstrip()}/index.csv.gz"
    if not exists(outfile): 
        print(f"File not found: {outfile}")
        sys.exit()

    if verbose:
        print(f"Read: {outfile}")

    # read main index file
    mainIndex = {}
    with gzip.open(outfile, 'rt') as f:
        for line in f:
            line = line.replace("\n","")
            lines = line.split("|")

            if len(lines) != 4:  
                print(f"Problems in reading index file: {outfile}")
                return

            mainIndex[lines[3]] = [
                lines[0],
                lines[1],
                lines[2]
            ] 

    ntot = 0
    globalIndex = {}

    # read data for specific publishers
    for key, xmap in mainIndex.items():

        pub_index = f"{basedirZWI}{key}.csv.gz" 
        if not exists(pub_index):
            print(f"File not found: {pub_index}, index is not valid!")
            return 

        articles = []
        with gzip.open(pub_index,'rt') as f:
            for line in f:
                line = line.replace("\n","")
                lines = line.split("|")
                
                if len(lines) != 3:
                    print(f"Problems reading index file: {pub_index}")
                    return None 

                articles.append(lines)

                # print(lines)
                ntot += 1

        #print(key,"gets",len(articles)," articles")
        globalIndex[key] = [
            xmap[0],
            xmap[1],
            xmap[2],
            articles
        ]

    if verbose:
        print(f"The size of local main index is {ntot} articles in {len(globalIndex)} publishers")

    return globalIndex

# Read global index files for all publishers from a remote URL of the phorm ZWI/en.
# The structure is: dic{key}=list
def getIndexRemote(
    baseURL: str,
    verbose: bool = True
    ) -> dict[str, str] | None:
    """ Return global index of all publishers from remote directory """

    if (not baseURL.endswith("en") and not baseURL.endswith("en/")):
        baseURL += "/en/"

    outfile = f"{baseURL.rstrip()}/index.csv.gz"

    # get main index first
    sessionTCP = requests.Session()
    if verbose:
        print(f"Read: {outfile}")

    web_response = sessionTCP.get(
        outfile,
        timeout=30, 
        stream=True
    )

    csv_gz_file = web_response.content # Content in bytes from requests.get
    f = io.BytesIO(csv_gz_file)
    
    mainIndex = {} # main index
    with gzip.GzipFile(fileobj=f) as fh:
        reader = csv.reader(
            io.TextIOWrapper(fh, 'utf8'),
            delimiter="|",
            quoting=csv.QUOTE_MINIMAL
        )

        for lines in reader:
            if len(lines) != 4:
                print(f"Problems in reading index file: {outfile}")
                return None

            mainIndex[lines[3]] = [
                lines[0],
                lines[1],
                lines[2]
            ]

    ntot = 0
    globalIndex = {}

    # read data for specific publishers
    for key, xmap in mainIndex.items():
        pub_index = f"{baseURL}/{key}.csv.gz"

        web_response = sessionTCP.get(
            pub_index,
            timeout=30, 
            stream=True
        )

        csv_gz_file = web_response.content # Content in bytes from requests.get
        f = io.BytesIO(csv_gz_file)
        articles=[]

        with gzip.GzipFile(fileobj=f) as fh:
            if verbose:
                print(f"Load= {pub_index}")

            reader = csv.reader(
                io.TextIOWrapper(fh, 'utf8'),
                delimiter="|",
                quoting=csv.QUOTE_MINIMAL
            )

            for lines in reader:
                if len(lines) != 3:
                    print(f"Problems reading index file: {pub_index}")
                    return None 

                articles.append(lines)

                # print(lines)
                ntot += 1

        #print(key,"gets",len(articles)," articles")
        globalIndex[key] = [
            xmap[0],
            xmap[1],
            xmap[2],
            articles
        ]

    sessionTCP.close()
    if verbose:
        print(f"The size of main index from {baseURL} is {ntot} articles in {len(globalIndex)} publishers")

    return globalIndex

def printGlobalIndex(
    gindex: dict,
    mess: str = ""
    ):
    """ print index in nice form"""

    gindex = {k: gindex[k] for k in sorted(gindex)}

    for key, data in gindex.items():
        print()
        print(
            mess,
            f"## publisher= {key} time= {data[0]} size= {data[1]} entries= {data[2]}"
        )

        articles = data[3]
        res = sorted(
            articles,
            key=lambda tup: tup[2]
        ) # list according to title 

        nn=1
        for a in res:
            print(f"{nn}) {a[0]} {a[1]} {a[2]}")
            nn += 1

##################################################################
#########  Calculate Delta, ie. difference betwen two global indexes
#########  Return download list
###################################################################
def getDeltaDifference(
    newIndex: dict,
    oldIndex: dict
    ) -> dict:
    """ Calculate delta between two global indexes. It returns a dictionary where publisher is key, but 
        list are missing titles.
    """

    # sorting according to keys
    oldIndex = {k: oldIndex[k] for k in sorted(oldIndex)}
    newIndex = {k: newIndex[k] for k in sorted(newIndex)}


    #dd1=newIndex["citizendium/citizendium.org"][3] 
    #dd2=oldIndex["citizendium/citizendium.org"][3] 

    #print(len(dd1),len(dd2))
    #print(len(newIndex["citizendium/citizendium.org"])) 
    #print(len(oldIndex["citizendium/citizendium.org"]))

    #print("New remote index=")
    #printGlobalIndex(newIndex,"New")
    #print("Old local index=")
    #printGlobalIndex(oldIndex,"Old")

    missing = {}
    for key in newIndex:
        # print("Key in new index=",key)
        # skip encyclopedias that have identical entries
        if len(key.strip()) < 1:
            continue

        # process publishers that are common for new and old publishers
        if key in oldIndex.keys():
            data1 = newIndex[key]
            data2 = oldIndex[key]

            # identical entries using newer timestamps. 
            if (int(data1[1]) == int(data2[1]) \
                and int(data1[2]) == int(data2[2]) \
                and int(data1[0]) <= int(data2[0])):
                    continue   

            # no timestamp requirement 
            #if (int(data1[1]) == int(data2[1])  and int(data1[2]) == int(data2[2])): continue
            #print("Process key in new index=",key)
            article1 = data1[3]  # list of articles in new sites 
            article2 = data2[3]  # list of articles in old site 
            # print(len(article1),len(article2))

            # convert article2 to dictionary
            olddic = {}
            for art in article2:
                olddic[art[2]] = (art[0], art[1]) 

            #print(olddic)
            #check missing for this publisher 
            missinglist=[]
            for art in article1:

                title = art[2]
                if title in olddic:
                    infodic = olddic[title]

                    if (int(art[1]) == int(infodic[1]) \
                        and int(art[0]) <= int(infodic[0])):
                            continue # check size and time in case of updates 

                missinglist.append([title, int(art[0])]) # article title and time      

            if len(missinglist) > 0:
                missing[key] = missinglist

            #print("Missing articles=",missinglist)

        else:  # this is a new publisher that does not exist!

            data1 = newIndex[key]
            article1 = data1[3]  # list of articles in new sites
            missinglist=[]

            for art in article1:

                missinglist.append([
                    art[2],
                    int(art[0])
                ]) # article title and time

            if len(missinglist ) > 0:
                missing[key] = missinglist

    return missing

def urlSafe(url: str) -> str:
    """Make URL safe for downloads"""

    chars = {
        '%': "%25",
        '#': "%23",
        '"': "%22",
        '(': "%28",
        ')': "%29",
        ' ': "%20"
    }

    for old, new in chars.items():
        url = url.replace(old, new)

    return url

###############################################################
################## remove ZWI files ###########################
###############################################################
def deleteZwiFiles(
    basedirZWI: str, 
    zwiFiles: list[str] = [],
    publist: list[str] = []
    ) -> None:
    """ Remove ZWI files from the disk and the index. Specify the list of ZWI files (without extension) and the publisher from which the files should be removed.  """

    globalIndex = getIndex(basedirZWI)
    if not globalIndex:
        return

    pubkeys = []
    pubdata = []

    # fetch the publisher
    for key, val in globalIndex.items():
        keys = key.split("/")
        if keys[0] in publist:
            pubdata.append(val)
            pubkeys.append(key) # name of the publisher/directory 

    if len(pubdata) == 0:
        print(f"i\nError: The publishers: {', '.join(publist)} are not found")
        return 

    err = ""
    ntot = 0
    nremove = 0

    # now scan all entries in the local index and remove the ZWI file
    # we also remove file phisically if there is anything to remove
    for xdir in pubkeys:
        pub_index = f"{basedirZWI}/{xdir}.csv.gz"
        # print("Read:",pub_index) 

        if not exists(pub_index):
            print(
                bcolors.WARNING 
                + f"\nError: File not found: {pub_index}. Abort removal for ZWI files" +
                bcolors.ENDC
            )

            return

        articles = []
        with gzip.open(pub_index,'rt') as f:
            for line in f:
                line = line.replace("\n", "")
                lines = line.split("|")

                if len(lines) != 3:
                    print(
                        bcolors.WARNING 
                        + f"\nError: Problem for reading the index file: {pub_index}" + 
                        bcolors.ENDC
                    )

                    return 

                zwiname = lines[2].strip()
                if zwiname in zwiFiles:
                    zwipath = f"{basedirZWI}/{xdir}/{zwiname}.zwi"

                    if exists(zwipath):
                        os.rename(
                            zwipath,
                            f"{basedirZWI}/trash/{zwiname}.zwi"
                        ) 

                        print(f"  -> ZWI file: {xdir}/{zwiname}.zwi removed to /trash/")
                        nremove += 1 
                        continue 

                    else:
                        err = f"\nWarning: File: {xdir}/{zwiname}.zwi was found in the index but not on the disk."
                        nremove += 1
                        continue 

                articles.append(lines)
                ntot += 1

        if len(err) > 1:
            print( bcolors.WARNING + err + bcolors.ENDC ) 

        if nremove == 0:
            print(
                bcolors.WARNING
                + "\nWarning: Nothing to remove. Index file is unchanged" + 
                bcolors.ENDC
            )

            return

        #print(key,"gets",len(articles)," articles")
        xmap = globalIndex[xdir]
        globalIndex[xdir] = [
            xmap[0],
            xmap[1],
            xmap[2],
            articles
        ]

    print(f"Nr of ZWI files removed: {nremove}")
    print(f"The size of main index is {ntot} articles in {len(globalIndex)} publishers")

    # now write the global index file
    writeIndexFiles(
        basedirZWI,
        globalIndex,
        publist
    )

    return


if __name__ == "__main__":

    #### input parameters ####
    kwargs = {}
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-i", "--input", 
        help="Input ZWI directory that ends as ZWI/en to create index files by rescanning directories"
    )

    parser.add_argument(
        "-p", "--publishers", 
        help="Update the index for the list of publishers by rescanning their directories. Global index will be updated too. The names of publishers should be separated by the commas"
    )

    parser.add_argument(
        "-d", "--delete", 
        help="Delete ZWI files and update the index. Global index will be updated too. The names of zwi file should be separated by the commas. Do not add extensions. You must to add one publisher from where the ZWI files should be remove. This operation does not involve re-scanning directories"
    )

    args = parser.parse_args()
    basedirZWI = ""

    if args.input:
        basedirZWI = args.input

    if args.publishers and not args.delete:
        if len(args.publishers) > 0:
            print(f"Update the existing index files for the publishers: {', '.join(args.publishers.split(','))}") 

    if args.delete:
        if len(args.delete) > 0:
            if args.publishers: 
                # if you made mistake and added .zwi extension, we correct for this
                filesRemove = []
                splitted = args.delete.split(",")

                for f in splitted:
                    if f.endswith(".zwi"):
                        filesRemove.append( f.replace(".zwi", "") )

                    else:
                        filesRemove.append( f.strip())

                print(f"Remove the list of ZWI files: {filesRemove} from publishers: {', '.join(args.publishers.split(','))}") 
                deleteZwiFiles(
                    basedirZWI, 
                    filesRemove, 
                    args.publishers.split(",")
                )

            else:       
                print(
                    bcolors.WARNING 
                    + f"\nError: These ZWI files will be removed: {', '.join(args.delete.split(','))} but the publisher is unknown. Specify the publisher!" + 
                    bcolors.ENDC
                )

            sys.exit()

    if len(basedirZWI) < 2:
        print("Did not specify the input ZWI directory? This should be the full path that ends with ZWI/en. Use --help")
        sys.exit()

    # excluded (optional) 
    excluded=[
        "en/sep/",
        "en/greatplains/",
        "en/jewishenc/",
        "en/whe/"
    ]

    # now make new index files for all publishers 
    if not args.publishers:
        createIndexFiles(basedirZWI, excluded) 

    else:
        publist = args.publishers.split(",")
        updateIndexFiles(basedirZWI, publist)
