#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This program is used to display a ZWI file and show it as HTML.
# Check arguments as: python3  ../../zwi_show.py -h
#
# @version 1.0, June 15, 2022 
# S.V.Chekanov 
# Organzation: KSF
# 

import sys, json, magic, mimetypes
import argparse, base64

from zipfile import ZipFile
from datetime import datetime
from bs4 import BeautifulSoup

# import htmlmin
# apt-get -y install python3-htmlmin

def guess_type(filepath: str) -> str:
    """
    Return the mimetype of a file, given it's path.
    This is a wrapper around two alternative methods - Unix 'file'-style
    magic which guesses the type based on file content (if available),
    and simple guessing based on the file extension (eg .jpg).
    :param filepath: Path to the file.
    :type filepath: str
    :return: Mimetype string.
    :rtype: str
    """

    try:
        return magic.from_file(filepath, mime=True)
    except ImportError:
        ftype = mimetypes.guess_type(filepath)[0]

        if not ftype:
            ftype = '.file' # fallback

        return ftype

def file_to_base64(filepath: str) -> str:
    """
    Returns the content of a file as a Base64 encoded string.
    :param filepath: Path to the file.
    :type filepath: str
    :return: The file content, Base64 encoded.
    :rtype: str
    """

    with open(filepath, 'rb') as f:
        encoded_str = base64.b64encode(f.read())

    return encoded_str.decode('utf-8')

#### input parameters ####
kwargs = {}
parser = argparse.ArgumentParser()
parser.add_argument("-q", "--quiet", action="store_true", help="don't show verbose")
parser.add_argument("-i", '--input', help="Input private key (to sign) or public key (to validate)")
parser.add_argument("-t", '--text', action="store_true", help="Output is a plain text")
parser.add_argument("-s", '--simple', action="store_true", help="Output is HTML but without images")

args = parser.parse_args()
args.verbose = not args.quiet

#print(" : Input ZWIfile =",args.input)
#print(" : Is verbose   =",args.verbose)

# time ISO 8601
now = datetime.now()
dt_string = now.strftime("%Y-%m-%dT%H:%M:%S.%f%z")
# print(dt_string)

# input ZWI file
zfile = args.input
unzipped_file = ZipFile(zfile, "r")
metadata = unzipped_file.read("metadata.json").decode('utf-8') 
js_metadata = json.loads(metadata)
js_content = js_metadata["Content"]

# get title etc 
title = js_metadata["Title"]
license = js_metadata["License"]
publisher = js_metadata["Publisher"]
timecreated = js_metadata["TimeCreated"]
dt_object = datetime.fromtimestamp(int(timecreated))
lastmod_date = dt_object.strftime('%Y-%m-%d')
categories = js_metadata["Categories"]
topics = js_metadata["Topics"]
source = js_metadata["SourceURL"]

if args.text:
    txt = unzipped_file.read("article.txt").decode('utf-8')

    txt = (
        f'{title}\n\n'
        f'From: {publisher}\n\n'
        f'{txt}\n'
        f'License: {license}\n'
        f'Source: {source}\n'
        f'Date: {lastmod_date}'
    )

    print(txt)
    sys.exit()

# images and other data  
media = unzipped_file.read("media.json")
js_media = json.loads(media)

# content 
htmlcontent = unzipped_file.read("article.html").decode('utf-8')
allmedia = allstyle = {}
for key, val in js_media.items():

    #print(key, val, guess_type(key))
    if key.endswith(".css"):
       allstyle[key] = ( unzipped_file.read(key) ).decode('utf-8') 
       #print(allstyle[key])
    else :
       idata = unzipped_file.read(key)
       allmedia[key] = base64.b64encode( idata ).decode('utf-8')

unzipped_file.close()

# add title if needed
# htmlcontent=htmlcontent.replace("<div class=\"container-fluid\">", "<div class=\"container-fluid\">\n<h1>"+title+"</h1>",1)
htmlcontent = htmlcontent.replace(
    "<body>",
    f"<body>\n<h1>{title}</h1><p>From: {publisher}</p>",
    1
)

htmlcontent = htmlcontent.replace(
    "</body>", 
    f"</body>\n<p>License: {license}<br>\nSource: {source}<br>\nDate: {lastmod_date}",
    1
)

soup = BeautifulSoup(
    htmlcontent,
    'html.parser'
)

# replace stylesheets
for style in soup.findAll("link", {"rel": "stylesheet"}):
    sty = style["href"]
    stylestring = allstyle[sty]

    newstring = f"<style type=\"text/css\" media=\"screen\">\n{stylestring}\n</style>" 

    style.replace_with(BeautifulSoup(newstring, 'html.parser'))

# extract images and add inline 
if args.simple:
    for img in soup.find_all('img'):
        img.decompose()

    final_html = str(soup)
    print(final_html)
    sys.exit()

# extract images and add inline 
for img in soup.find_all('img'):
    img_path = img.attrs['src']
    mimetype = guess_type(img_path)

    data = None
    try:
        data = allmedia[img_path]
    except KeyError:
        continue 

    img.attrs['src'] = f"data:{mimetype};base64,{data}"

# string output
final_html = str(soup)

# minified = htmlmin.minify(final_html,remove_empty_space=True)
print(final_html)