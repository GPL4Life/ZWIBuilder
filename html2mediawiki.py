#!/usr/bin/env python
# Filter to strip the header and footer stuff from the cached page

import sys, re

from bs4 import BeautifulSoup
from bs4 import Tag, NavigableString, Comment

# compile patterns
r = re.compile("^http://")
rr1 = re.compile(" ")
r2 = re.compile("/[A-Z][a-z_0-9-]*")
r3 = re.compile(r"^http://ascendcode.cheme.cmu.edu/viewvc.cgi/code/(.*)$")
r3trunk = re.compile(r"trunk/(.*)\?view=markup$")
r3branch = re.compile(r"branches/([^/]+)/(.*)\?view=markup$")
r3dir = re.compile(r"trunk/(.*)")
r4 = re.compile(r"^([^)]+)\s+\(page does not exist\)$")
r5 = re.compile("<!-- BEGIN BODY -->(.*)<!-- END BODY -->", re.S)

def replace_templates(soup):

    templates = soup.findAll(
        "div",
        {"id":"task", "class":"notice metadata"}
    )

    for t in templates:
        t.replaceWith(NavigableString("{{task}}"))

def strip_contents(soup):

    c = soup.find(
        "table",
        {'id':'toc', 'class':'toc', 'summary':'Contents'}
    )

    if c:
        c.extract()

def strip_wiki_comments(soup):

    msg1 = "NewPP limit report"
    l1 = len(msg1)

    msg2 = "Saved in parser cache"
    l2 = len(msg2)

    #print "STRIPPING WIKI COMMENTS"
    def co(tag: Tag) -> bool:
        if isinstance(tag, Comment) and tag:
            value = tag.string
            if not value:
                return False

            if value.strip()[0:l1] == msg1 \
                or value.strip()[0:l2] == msg2:

                #print "COMMENT:",tag.string.strip()
                return True

        return False

    for c in soup.findAll(text=co):
        c.extract()

def strip_script(soup):

    for s in soup.findAll('script'):
        s.extract()

def strip_highlight(soup):

    for a1 in soup.findAll('p'):
        if a1.find('style',{'type':"text/css"}):

            n1 = a1.nextSibling
            if str(n1.string).strip() != "/* Highlighting theme definition: */":
                #print "NO MATCH"
                sys.exit(1)

            n2 = n1.nextSibling
            #print "REMOVING",str(a1)
            a1.extract()

            #print "REMOVING N1",str(n1)
            n1.extract()

            n3 = n2.nextSibling
            #print "REMOVING N2",str(n2)
            n2.extract()

            n4 = n3.nextSibling
            #print "REMOVING N3",str(n3)
            n3.extract()

            pre = n4.nextSibling
            #print "REMOVING N4",str(n4)
            n4.extract()

            if pre.name != "pre":
                #print "ERROR parsing syntax-highlighting:",pre
                sys.exit(1)

            for x in pre.findAll("b", {"style": True}):

                x.replaceWith(
                    NavigableString(str(x.string))
                )
    
            for x in pre.findAll("span", {"class": True}):
                x.replaceWith(
                    NavigableString(str(x.renderContents()))
                )

            t = Tag(soup, "source", [("lang",'a4c')])
            t.insert(
                0, 
                NavigableString(
                    str(pre.renderContents()
                ).strip())
            )

            pre.replaceWith(t)

def strip_anchors(soup):

    for a1 in soup.findAll("a", {"name": True}):
        #print "ANCHOR:",a1
        a1.extract()

def wikify_headings(soup):

    for h in soup.findAll(["h1", "h2", "h3", "h4", "h5", "h6"]):
        if not h.find("span", {"class": "mw-headline"}):
            #print "HEADING: SKIPPING:",h
            continue

        #print "HEADING:",h
        level = int(str(h.name)[1:])

        h2 = NavigableString(
            f"\n={level + h.span.renderContents()}={level}"
        )

        h.replaceWith(h2)

def wikify_paragraphs(soup):

    for p in soup.findAll('p'):
        #print "PARA",str(p)
        if not p.renderContents():
            p.replaceWith(NavigableString("\n"))

        else:
            p.replaceWith(NavigableString(f"\n{p.renderContents()}"))

def strip_printfooter(soup):
    soup.find(
        "div",
        {"class": "printfooter"}
    ).extract()

def strip_wikicomments(soup):

    return

def wikify_categories(soup):

    cats = soup.find("div", {"id": "catlinks"})
    if not cats:
        return

    cc = []
    for a in cats.findAll("a"):
        if str(a['href']) == "/Special:Categories":
            #print "CATEGORIES LINK ignored"
            a.extract()

        elif r2.match(a["href"]):
            t = NavigableString(f"[[{a['href'][1:]}]]\n")
            #print "  categ:",t.strip()
            cc.append(t)

    #print "CATS:",cc
    #cats.replace(cc)
    for c in cc:
        cats.parent.append(c)

    cats.extract()

def wikify_images(soup):

    for a in soup.findAll("a", {"class": "image"}):
        if a.img:
            if a.img["alt"][0:6] == "Image:":
                #print "IMG1",a.img['alt'][6:]
                a1 = NavigableString(f"[[Image:{a.img['alt'][6:]}]]")
                #print "-->",a1
                a.replaceWith(a1)

            elif a["href"][0:6] == "/File:":
                #print "IMG",a['href'][6:]
                a1 = NavigableString(f"[[Image:{a['href'][6:]}]]")
                a.replaceWith(a1)
                #print "-->",a1
            else:
                sys.stderr.write(f"CAN'T PROCESS IMAGE LINK {str(a)}\n")

def wikify_math(soup):
    for img in soup.findAll("img", {"class": "tex"}):
        s = f"<math>{img['alt']}</math>"
        #print "MATH:",s

        img1 = NavigableString(s)
        img.replaceWith(img1)

        #img.replaceWith(NavigableText(s))

def wikify_indents(soup):
    for dl in soup.findAll("dl"):
        s = ""

        for dd in dl.findAll("dd"):
            s += f":{dd.renderContents()}\n"

        dl1 = NavigableString(s)
        dl.replaceWith(dl1)        

def wikify_links(soup):

    def linkified(s):
        s = rr1.sub("_", s)
        s = s[0:1].upper() + s[1:]

        return s

    for a in soup.findAll("a", {"href": True}):

        #print "LINK:",a.parent
        m3 = r3.match(a["href"])
        if m3:
            t1 = m3.group(1)
            m3 = r3trunk.match(t1)

            if m3:
                t = NavigableString("{{src|%s}}" % m3.group(1))
                a.replaceWith(t)

            else:
                m3 = r3branch.match(t1)
                if m3:
                    t = NavigableString(
                        "{{srcbranch|%s|%s}}" % (m3.group(1), m3.group(2))
                    )

                    a.replaceWith(t)

                else:
                    m3 = r3dir.match(t1)
                    if m3:
                        t = NavigableString("{{srcdir|%s}}" % m3.group(1))
                        a.replaceWith(t)
                        
                    else:
                        t = NavigableString(f"[{a['href']} {a.renderContents()}]")
                        a.replaceWith(t)

            #print "LINK:",t
        elif r.match(a["href"]):
            if a["href"] == a.renderContents():
                t = NavigableString(f"[{a['href']}]")

            else:
                t = NavigableString(f"[{a['href']} {a.renderContents()}]")

            a.replaceWith(t)
            #print "LINK:",t

        elif r2.match(a["href"]):
            if linkified(a.renderContents()) == a['href'][1:]:
                t = NavigableString(f"[[{a.renderContents()}]]")

            else:
                t = NavigableString(f"[[{a['href'][1:]}|{a.renderContents()}]]")

            a.replaceWith(t)
            #print "LINK:",t

        else:
            m4 = r4.match(a["title"])
            if m4:
                t = NavigableString(f"[[{m4.group(1)}]]")
                a.replaceWith(t)

def wikify_bold(soup):
    for b in soup.findAll("b"):
        #print "BOLD:",b
        b2 = NavigableString(f"'''{b.renderContents()}'''")
        b.replaceWith(b2)

def wikify_italics(soup):
    for i in soup.findAll("i"):
        i.replaceWith(f"''{i.renderContents()}''")

def wikify_list(l, prefix="*"):
    #print "WIKIFY L:",l.prettify()
    s = ""
    for tag in l.findAll(["ul", "ol", "li"]):
        if tag.name == "ul":
            s += wikify_list(tag, f"{prefix}*")

        elif tag.name == "ol":
            s += wikify_list(tag, f"{prefix}#")

        elif tag.name == "li":

            # sometimes nested lists are incorrectly placed within a <li>
            #print "STUFF IN LI:"
            if tag.findAll(["ol","ul"]):
                for stuff in tag:

                    if isinstance(stuff, Tag) and stuff.name == "ol":
                        s += wikify_list(stuff, f"{prefix}#")

                    elif isinstance(stuff, Tag) and stuff.name == "ul":
                        s += wikify_list(stuff, f"{prefix}*")

                    elif isinstance(stuff,NavigableString) and stuff.string.strip():
                        s += f"\n{prefix} {stuff.string.strip()}"

            else:
                s += f"\n{prefix} {tag.renderContents().strip()}"

    #print "\n\nRESULT OF WIKIFY L:",s,"\n\n"

    return s

def wikify_lists(soup):

    # FIXME handle nested lists!
    for ul in soup.findAll("ul"):
        ul.replaceWith(
            NavigableString(wikify_list(ul, "*"))
        )

    for ol in soup.findAll("ol"):
        ol.replaceWith(
            NavigableString(wikify_list(ol, "#"))
        )

def wikify_tables(soup):
    for ta in soup.findAll("table"):
        s = "\n{| class=\"wikitable\"\n"

        for tr in ta.findAll("tr"):
            s += "|-\n"

            for t in tr.findAll(["td", "th"]):
                if t.name == "td":
                    s += f"| {t.renderContents()}"

                else:
                    s += f"! {t.renderContents()}"

        s += "|}"
        ta.replaceWith(NavigableString(s))

def html2wiki(html,wikiname):
    """
    This is the main function that converts an HTML string into corresponding wiki syntax.
    It expects a full HTML page including header, footer, etc, not just the 'content' section
    of the page.

    @param html the raw 'page source' input (eg from Google Cache)
    @param wikiname the name of the wiki from which the content is derived
    """
    s = BeautifulSoup(html, "html.parser")

    title = s.title
    if not title:
        title = ""

    else:    
        title = title.string
    
    if not title:
        title = ""

    if (title[-9:] != f" - {wikiname}"):
        print (f"Incorrect title '{title}'") 
        #sys.exit(1)

    title = title[:-9]
    print (f"  page title= '{title}'") 

    pagecontent = r.search(html).group(1)

    #print "Parsing page content..."
    s1 = BeautifulSoup(pagecontent, "html.parser")

    replace_templates(s1)
    strip_contents(s1)
    strip_wiki_comments(s1)
    strip_script(s1)
    strip_printfooter(s1)
    strip_highlight(s1)
    strip_anchors(s1)
    wikify_headings(s1)
    wikify_paragraphs(s1)
    wikify_categories(s1)

    s1 = BeautifulSoup(str(s1), "html.parser")
    wikify_bold(s1)

    s1 = BeautifulSoup(str(s1), "html.parser")
    wikify_italics(s1)

    s1 = BeautifulSoup(str(s1), "html.parser")
    wikify_images(s1)
    wikify_math(s1)
    wikify_indents(s1)

    wikify_links(s1)

    wikify_lists(s1)
    wikify_tables(s1)

    # TODO: do something to catch 'texhtml'?

    return str(s1), title

if __name__=="__main__":
    sys.stderr.write(f"Reading file {sys.argv[1]}...\n")

    with open(sys.argv[1]) as fd:
        data = fd.read()

    print(html2wiki(data, "ASCEND"))