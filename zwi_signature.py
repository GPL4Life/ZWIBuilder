#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This program is used to sign ZWI file with a private key of the ZWI publisher.
# It can also remove the key ("unsign"). After a ZWI file is signed, one can validate 
# the signature (and the validity of the ZWI file) using a public key.
# The public key can be in a local file, or, if not found, can be located at remote PSQR. 
# Check arguments as: python3  ../../zwi_signature.py -h
# The program expects an input ZWI file, and 2 public (auth.pub) and private (auth.pem) keys.
#
# @version 1.0, June 8, 2022 
# S.V.Chekanov, L.Sanger, C.Gribneau  
# Organzation: KSF
# 

import os, sys, json
import hashlib, requests, argparse

from zipfile import ZipFile
from os.path import exists
from datetime import datetime

try:
    from jose import jwt
except ImportError:
    print("Error: Please install jose module https://github.com/mpdavis/python-jose\n> pip install python-jose")
    sys.exit()

# hashing algorithm
hashing_algo = hashlib.sha1

# Task: Look inside the ZWI file and validate all  hashes.
# If everything is OK, return True. Otherwise, return False.
# This is a slow, but it examines all files as needed.

nmeta = 0
nmedia = 0

def makeHash(
    data: str | bytes, 
    encoding: str = 'utf-8'
    ) -> str:

    # encode if needed
    if isinstance(data, str):
        data = data.encode(encoding)

    return hashing_algo(
        data
    ).hexdigest()

def validate_files(unzipped_file, js_content, js_media):
    global nmeta, nmedia

    for key, val in js_content.items():
        nmeta += 1

        #print(key, val)
        name = unzipped_file.read(key)
        digest = makeHash(name)

        if digest !=  val:
            print(f"Error: Hash of the content {key} is wrong")
            return False 

    # if no media file
    if not js_media: 
        return True

    for key, val in js_media.items():
        nmedia += 1

        #print(key, val)
        name = unzipped_file.read(key)
        digest = makeHash(name)

        if digest !=  val:
            print(f"Error: Hash of the media {key} is wrong")
            return False 

    return True

def find_between( s, first, last ):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )

        return s[start:end]
    except ValueError:
        return ""

#### input parameters ####
kwargs = {}
parser = argparse.ArgumentParser()
parser.add_argument('-q', '--quiet', action='store_true', help="don't show verbose")
parser.add_argument("-z", '--zwi', help="Input ZWI file. Output will be the same as input")
parser.add_argument("-d", '--dir', help="Input directory with unzipped ZWI file (for fast validation)")
parser.add_argument("-i", '--input', help="Input private key (to sign) or public key (to validate)")
parser.add_argument("-p", '--organization', help="Name of the organization (producer) of the input ZWI file")
parser.add_argument("-a", '--address', help="Address of the producer of the input ZWI file")
parser.add_argument("-k", '--kid', help="PSQR a key ID of decentralized identifiers (DIDs)")
parser.add_argument("-u", '--url', help="Validate using the URL of the publisher with the public key")
parser.add_argument("-v", '--validate', action='store_true', help="Validate with public key \"auth.pub\"")
parser.add_argument("-r", '--remove', action='store_true', help="Remove the signature from the ZWI file")
parser.add_argument("-f", '--full', action='store_true', help="Full check of all files inside ZWI (slow)")

# algorithm in use
alg = "RS256"

args = parser.parse_args()
args.verbose = not args.quiet
fast = not args.full
# print(fast,args.full)

if args.zwi:
    if len(args.zwi) > 4:
        print(f" : Input ZWI file= {args.zwi}")

if args.dir:
    if len(args.dir) > 4:
        print(f" : Input directory with unzipped ZWI= {args.dir}")
        fast = True

signature_file = "signature.json"
if args.remove:

    z = ZipFile(args.zwi,"r")
    try:
        sig = z.read(signature_file)
    except KeyError as e:
        print()
        print("Error: This file is not signed. Exit!")
        sys.exit()

    z.close()

    print(f"Remove signature= {args.remove}")

    cmd = f"zip -d {args.zwi} {signature_file}"
    os.system(cmd)

    print("Done! Exit.")
    sys.exit()

if args.validate:
    unzipped_file = None 

    if (not args.dir and args.zwi):
        unzipped_file = ZipFile(args.zwi, "r")

        sig =      unzipped_file.read(signature_file)
        metadata = unzipped_file.read("metadata.json")

        data_metadata = makeHash(metadata)

        # media
        media = None
        data_media = None
        try:
            media = unzipped_file.read("media.json")
            data_media = makeHash(media)
        except KeyError as e:
            pass

    sig = None
    metadata = media = ''
    data_metadata = data_media = ''

    if (args.dir and not args.zwi):
        #print(" : Reading local file:",args.dir +'/' + signature_file)
        #print(" : Reading local file:",args.dir +'/' + 'metadata.json')
        #print(" : Reading local file:",args.dir +'/' + 'media.json')
        with open(f"{args.dir}/{signature_file}", 'r') as f:
            sig = f.read()

        with open(f"{args.dir}/metadata.json",'r') as f:
            metadata = f.read()
            data_metadata = makeHash(metadata)

        with open(f"{args.dir}/media.json",'r') as f:
            media = f.read()
            data_media = makeHash(media)

    if sig:
        try: 
            signature = json.loads(sig)
            org = signature["identityName"]
            address = signature["identityAddress"]
            time = signature["updated"]
            token = signature["token"]

            if "alg" in  signature:
                ralg = signature["alg"]

                if len(ralg) > 2: 
                    alg = ralg
                    print(f" : Using algorithm= {alg}")

            psqrKID = ""
            if "psqrKid" in signature:
                psqrKID = signature["psqrKid"]

            # the input was given instead?
            if args.kid: 
                psqrKID = args.kid 

            if args.url:
                psqrKID = args.url

            # read public key      
            publicKey = ""
            if "publicKey" in signature:
                publicKey = signature["publicKey"]

            local_file = False
            if args.input: 
                local_file = exists(args.input)

            # start from none
            decoded_data = None

            # first check is any local pub file (to speed up)
            if (local_file and decoded_data): 
                print(f" : Using local public key file= {args.input}")

                with open(args.input) as pubkey_file:
                    decoded_data = jwt.decode(
                        token,
                        key=pubkey_file.read(), 
                        algorithms=alg
                    )

            # get locally stored public key
            if (len(publicKey) > 390 and not args.kid and not args.url):
                local_file = False
                print(" : Using stored public key from ZWI")

                #print(publicKey)
                decoded_data = jwt.decode(
                    token, 
                    key=publicKey, 
                    algorithms=alg
                )
                #print("decoded",decoded_data)

            if ( (not local_file and not decoded_data) or args.kid or args.url):

                # convert to standard URL
                if args.kid:
                    print(f" : Using remote public key from psqrKid= {psqrKID}")

                if args.url: 
                    print(f" : Using remote public key from URL= {args.url}")

                xurl = psqrKID.replace("did:psqr:","https://")
                xurl = xurl.replace("#publish","auth.pub")

                tcpSession = requests.Session()
                tcpSession.headers = {
                    "accept": (
                        "text/html,"
                        "application/xhtml+xml,"
                        "application/xml;q=0.9,"
                        "image/avif,"
                        "image/webp,"
                        "image/apng,"
                        "*/*;q=0.8,"
                        "application/signed-exchange;v=b3;q=0.9"
                    ),

                    "accept-encoding": "gzip, deflate",
                    "accept-language": "en-GB,en-US;q=0.9,en;q=0.8",
                    "cache-control": "max-age=0",
                    "sec-fetch-dest": "document",
                    "sec-fetch-mode": "navigate",
                    "sec-fetch-site": "none",
                    "sec-fetch-user": "?1",
                    "sec-gpc": "1",
                    "dnt": "1",
                    "upgrade-insecure-requests": "1",
                    "user-agent": (
                        "Mozilla/5.0 "
                        "(Windows NT 10.0; Win64; x64) "
                        "AppleWebKit/537.36 "
                        "(KHTML, like Gecko) "
                        "Chrome/100.0.4896.79 "
                        "Safari/537.36"
                    )
                }
                
                try:
                    r = tcpSession.get(xurl) 
                    decoded_data = jwt.decode(
                        token, 
                        key=r.text, 
                        algorithms=alg
                    )

                    #print(r.content)
                except requests.exceptions.RequestException as e:  # This is the correct syntax
                    print(e)

                tcpSession.close()

            if decoded_data:
                #print()
                #print("Decoded data with the  public key:")
                #print(decoded_data)

                metadata_read = decoded_data["metadata"]
                media_read = None
                if "media" in decoded_data: 
                    media_read = decoded_data["media"]

                authority_read = decoded_data["authority"]
                # print(data_metadata+" "+metadata_read)
                # print(data_media+" "+media_read)
                isValid = 0
                if data_metadata != metadata_read: 
                    isValid = 1

                if media_read: 
                    if data_media != media_read:
                        isValid = 2

                if signature["identityName"] != authority_read["identityName"] \
                    or signature["updated"] != authority_read["updated"] \
                    or signature["identityAddress"] != authority_read["identityAddress"]:
                    isValid = 3

                # validate all sha1 files?
                if not fast:
                    js_metadata = json.loads(metadata)
                    js_media = None

                    if media: 
                        js_media = json.loads(media) 

                    js_content = js_metadata["Content"]
                    hashCorrect = validate_files(
                        unzipped_file, 
                        js_content, 
                        js_media
                    )

                    if not hashCorrect:
                        print("Error: Hash for some files are wrong! Exit!")
                        isValid = 4

                if isValid == 0:
                    print(f" : Signing organization= {signature['identityName']}")
                    print(f" : Address of signing organization= {signature['identityAddress']}")
                    print(f" : Time when it was signed= {signature['updated']}")

                    if not fast:
                        print(f" : Nr of files verified= {nmeta} (metadata), {nmedia} (media)")

                    print(f" : Is fast validation= {fast}")
                    print(" == Signature is valid! == ")
                    sys.exit()

                if isValid == 1:
                    print("")
                    print(" Error: Inconsistent hash of metadata")

                if isValid == 2:
                    print("")
                    print(" Error: Inconsistent hash of media data")

                if isValid == 3:
                    print("")
                    print(" Error: Information stored in token is not consistent with human-readable info")
                    print(" Error: This indicate that somebody tried to change signature.json")

                if isValid == 4:
                    print("")
                    print(" Error: Inconsistent hashes of stored files")

                if isValid != 0:
                    print("== Signature is NOT valid! == ")
                    sys.exit()

            else:
                print(" Error: jwt.decode returns empty data")
                sys.exit()

        except KeyError as e:
            print(f"Key error: {str(e)}")
            print("Error: This file is not signed. Exit!")
            sys.exit()

    if unzipped_file: 
        unzipped_file.close()

    sys.exit()

print(f" : Signing organization= {args.organization}")
print(f" : Address of signing organization= {args.address}")
print(f" : PSQR key ID= {args.kid}")
print(f" : Is verbose= {args.verbose}")
print(f" : Is fast validation= {fast}")

# time ISO 8601
now = datetime.now()
dt_string = now.strftime("%Y-%m-%dT%H:%M:%S.%f%z")

# input ZWI file
zfile = args.zwi
unzipped_file = ZipFile(zfile, "r")
metadata = unzipped_file.read("metadata.json")
js_metadata = json.loads(metadata)
js_content = js_metadata["Content"]

# check if exists
js_media = media = None
try:
    media = unzipped_file.read("media.json")
    js_media = json.loads(media)
except KeyError as e:
    pass 
    #print("Error: This file does not have media.json",e)

# validate all sha1 files? 
if not fast: 
    hashCorrect = validate_files(
        unzipped_file, 
        js_content, 
        js_media
    )

    if not hashCorrect:
        print("Error: Hash for some files are wrong! Exit!")
        sys.exit(1)

# data to decode
data = {}

# who signed added
data["authority"] = {
    "identityName": args.organization, 
    "identityAddress": args.address, 
    "psqrKid": args.kid,
    "updated": dt_string
} 

data["metadata"] = makeHash(metadata)

if js_media and media:
    data["media"] = makeHash(media)

if (args.verbose):
    print()
    print(f"Data that goes to token of {signature_file}:")
    print(data)

unzipped_file.close()

# Encode data with private key
file_exists = exists(args.input)
if not file_exists:
    print(f"Error: No private key was found in {args.input}")
    sys.exit()

with open(args.input) as key_file:
    token = jwt.encode(
        data, 
        key=key_file.read(), 
        algorithm=alg
    )

# we also read public key and insert it
pubFile = args.input.replace(".pem",".pub")
file_exists = exists(pubFile)

if not file_exists:
    print(f"Error: No public key was found in {pubFile}")
    print(f"Error: We include public key in the signature to make decentralized verification possible.")
    sys.exit()

print(f" : Private key used= {args.input}")
print(f" : Public key included= {pubFile}")
print(f" : Used algorithm= {alg}")
print(f" : Signature time= {dt_string}")

pubkey = ""
with open(pubFile, 'r') as f1:
    pubkey = f1.read()
    #pubkey=pubkey.replace("-----BEGIN PUBLIC KEY-----", "")
    #pubkey=pubkey.replace("-----END PUBLIC KEY-----", "")
    #pubkey=pubkey.replace("\n", "")
    #pubkey=pubkey.strip()

if args.verbose:
    print()
    print("Created token:")
    print(token)

json_string = {
    "identityName": args.organization, 
    "identityAddress": args.address, 
    "psqrKid": args.kid, 
    "token": token, 
    "publicKey": pubkey,
    "alg": alg,
    "updated": dt_string
}

with open(signature_file, 'w') as outfile:
    outfile.write(
        json.dumps(
            json_string, 
            indent=4
        )
    )

# decode data:
# with open(args.input) as pubkey_file:
#    decoded_data = jwt.decode(token, key=pubkey_file.read(), algorithms='RS256')

#if (args.verbose):
#  print()
#  print("Decoded data with the  public key:")
#  print(decoded_data)


print(f"Signature {signature_file} was inserted")
os.system(f"zip -q {zfile} {signature_file}")
os.remove(signature_file)

