#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Create ZWI files from Wikipedia 
# @version 1.0. Dec 10, 2021
# S.V.Chekanov (KSF)

import requests, gzip, argparse
from zwi_producer import *

# Please change these lines for other wiki based on Mediawiki 
SITE = "Wikipedia"  # Full title of the Wiki 
SITE_API="https://en.wikipedia.org/w/" # Should Points to api.php 
endPoint = f"{SITE_API}api.php"
SITE_URL = "https://en.wikipedia.org/wiki/" # Where all articles are located (SITE_URL+title) 
SITE_SHORT = "wikipedia"                    # short name  

##################### do not change below ##################

# alway use low case for directories
SITE_SHORT = SITE_SHORT.lower()

#### input parameters ####
kwargs = {}
parser = argparse.ArgumentParser()
parser.add_argument(
    "-q", "--quiet", 
    action="store_true", 
    help="don't show verbose"
)

parser.add_argument(
    "-t", "--title", 
    help="Title of the article"
)

args = parser.parse_args()
args.verbose = not args.quiet

if not args.title:
    print('Please specify an article title!')
    exit()

print(f"Title= {args.title}")
print(f"Is verbose= {args.verbose}")

title = args.title

PARAMS = {
    "action": "parse",
    "page": title,
    "prop":"text|wikitext",
    "format": "json"
}

session = requests.Session()
resp = session.get(
    url=endPoint, 
    params=PARAMS
)

DATA = resp.json()

output_html = DATA["parse"]["text"]['*'] 
output_wikitext = DATA["parse"]["wikitext"]['*']

output_new = output_html.replace(
    "/w/index.php", 
    f"{SITE_API}index.php"
)

for old, new in {
        "/wiki/": SITE_URL,
        "[math]": "\(",
        "[/math]": "\)",

        #add proper https
        "src=\"//": "src=\"https://",
        "srcset=\"//": "srcset=\"https://",
        "url(\"//": "url(\"https://"
        }.items():

    output_new = output_new.replace(old, new)

with gzip.open("article.html.gz",'wb') as fout:
    fout.write( output_new.encode() )

with gzip.open("article.wikitext.gz",'w') as fout:
    fout.write( output_wikitext.encode() )

html_file = "article.html.gz"
title = title.replace(" ","_")

zwi_file = f"{title}.zwi" 
zwi_file = urllib.parse.quote_plus(zwi_file)

makeZWI(
    html_file, 
    zwi_file, 
    title, 
    SITE_SHORT
)