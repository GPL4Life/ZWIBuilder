# Introduction to ZWIBuilder

ZWI files (https://handwiki.org/wiki/ZWI_file_format) are zip-compressed HTML files (with actual articles in different formats, media files and JSON metadata with descriptions) used for storing complete encyclopedic articles offline.
The file format is similar to HTMLZ or ePub used by e-book readers. However, ZWI are tuned to keep encyclopedia articles, especially articles that are created using Wiki-style programs (Mediawiki, TWiki, Dokuwiki) that use Wikitext (or Wikicode) 
as the primary source for content.

ZWI files can be created using ZWI-export plugins developed for Mediawiki and Dokuwiki (they already exist). When using these ZWI-plugins for wiki-platforms, the created ZWI file includes the original Wikitext/Wikicode, all revisions  and the parsed HTML file 
(with images). For such plugins, Wikitext is the primary source, while the HTML is a ``derivation''  created by these encyclopedia after parsing Wikitext. The reason for storing HTML in sush files is due to the fact that creating readable HTML from Wikitext can only be done correctly the original wiki platforms since they may use a large number of templates and various plugins. Therefore, such Wikitext-HTML parsing is difficult to achieve using external programs. 

However, not every online encyclopedia can (or may want) integrate such ZWI-export plugins. Therefore, it is necessary to create a program that creates ZWI files directly from the HTML provided by external encyclopedias. This will also allow to create ZWI files for articles from encyclopedias that do not use Wikitext.

Once such ZWI files are created, encyclopedia articles can be "detached" from MySQL-style databases of original encyclopedias, and may have their own "life". ZWI files can be:

- viewed offline
- distributed over the network or any torrent-like  distribution systems
- visualized using desktop or smartphone applications 
- edited using offline or online HTML-style editors. 
- imported to alternative encyclopedic platforms
- used for archiving and historic preservation

Some of such tools will be developed in future. 

**ZWIBuilder** is a program that creates ZWI files from  external HTML files without the ZWI-export plugins, i.e. without a direct access to Wikitext and internal API of Wiki content management systems. The goal of the ZWIBuilder program is to create self-contained HTML files with encyclopedic articles and all necessary media.

Currently, this program only works with WP. It needs to be validated for other encyclopedias. The examples in this documentation were developed using Linux.

# What do you need 

You need Python3, Bash (which are typically available under Linux/Mac) and Beautiful Soup Python library.
In addition, install the JOSE library which is only needed for signing ZWI files. For Ubuntu,   run
 
```
sudo apt-get -y install python3-jose
```


# How it works

The program fetched cached (compressed) HTML files from encycloreader.org, process such cached files, download the required images, encloses the body of article between _html_header.html_ and _html_footer.html_ and ZIPs all files to create a file with the extension *.zwi. It also adds additional information (metadata) as JSON files:

- media.json - list of included media files
- metadata.json - dictinary with metadata. A simplest structure looks like this:

```
{"ZWIversion": "1",  "Title": "Standard_model",  "CreatorName": "Wikipedia",  
"Primary": "article.html",  "LastModified": "1632928547"}
```
The field "Primary" indicates that the the primary imported content is HTML (article.html). For a typical ZWI file created using Wiki-program plugins, the primary content is "article.wikitext" (Wikicode), while article.html is a derivation created by Wiki engines. This dictionary can be extended. These metadata files can be used by external programs to visualize the articles.


# Testing this tool

Here are the steps to create a ZWI file using external encyclopedia:

(1) First, search for a word using encycloreader.org and find some article from WP (or similar encyclopedia supported by encycloreader. Look at this article using a browser. You may also use "curl" or "wget" command 

```bash
curl -L https://encycloreader.org/r/wikipedia.php?q=TITLE
```

where "TITLE" is known title of article that exists.
This step prepares the article's HTML, checks internal links and create well-formatted document structure on encycloreader.org. It also creates various cached images for equations etc. on the external source. 

(2) Run this program as:

```bash
cd ZWIBuilder
chmod 755 *
./ARUN [URL] 
```

where URL points to the URL of the article viewed in encycloreader.org. Note that this can only work if you run this program within about 1 h after viewing an article on encycloreader.org since cached images may be removed  on the external source. For example, first view this article using a Web browser:

```
https://encycloreader.org/r/wikipedia.php?q=Quark
```

After viewing it, encycloreader.org creates a small cache file that is suitable for further processing.  Then, make the ZWI file as:

```
ARUN "https://encycloreader.org/r/wikipedia.php?q=Quark"
```

This will create Quark.zwi.  The program also attemts to view this article in firefox (press "y" at the end).

If you want to view this article manually, just do this:


```bash
unzip Quark.zwi
firefox article.html
```
Note that article.html does not contain the title. The title can be included from meta.json using external programs used to visualize the article (since titles can be futher "decorated").

According to the ZWI-format specification, all images should be located in "data/media/images". Unlike the original ZWI files used in Wiki-software plugins, this ZWI file does not include the Wikitext. meta.json tells that the primary source of the article is _article.html_, not _article.wikitext_. The latter can be added in future (or created from HTML), but this may not be necessary since the correct transformation between HTML and Wikitext can only be done by the original external encyclopedias.

Note that the included "meta.json" file has a very basic structure. It will be used
for external program to visualize this article (add titles, time, authors etc). 
It can easily be extended by adding more metadata entries 
(like votes, etc).

# Making ZWI without EncycloReader

An example of making ZWI files without the EncycloReader is provided below (for Wikipedia):

```bash
python3 zwi_wikipedia.py -t Quantum_dot
```
The last argument is the title of the article from Wikipedia.
This creates the ZWI file called "Quantum_dot.zwi". For other Mediawiki websites, you need to modify the variables SITE, SITE_API, SITE_URL and SITE_SHORT in the file "zwi_wikipedia.py

This program is in progress.


# How to sign a ZWI file 

ZWI files are signed by adopting elements of the PSQR DID (Decentralized Identifiers for Public Squares), https://vpsqr.com/did-method-psqr/v1/
At this moment, ZWI signature process does not follow the exact technical
implementation of PSQR DID since ZWI files are complex binary data structures that require the appropriate tools. 
However, the used tokens and verification
are complaint with JSON Web Tokens (JWT) used by PSQR DID. 

A signed ZWI file includes a JSON file called "signature.json". This file has a human-readable part and an encrypted "token"
that contains a dictionary with the human-readable part and the hashes of metadata.json and media.json that keep track of the hashes of all files inside a 
ZWI file. The file "signature.json" has this structure: 

```json
{
    "identityName": "KSF",
    "identityAddress": "USA",
    "psqrKid": "did:psqr:[DOMAIN]/[USER]/#publish",
    "token": "eyJhbGciOiJSUz ... ",
    "publicKey": "-----BEGIN PUBLIC KEY-----\n ...", 
    "alg": "RS256",
    "updated": "2022-09-02T17:03:06.350063"
}
```
here did:psqr:[DOMAIN]/[USER]/#publish points to the location of the public key ([DOMAIN] and [USER] are the domain name and the user/publisher). 
The signature includes the human readable attributes such as the publisher name ("KSF"), address ("USA") and the URL location of the
PSQR.

To sign, validate and remove the signature from a ZWI file, use this command:

```bash 
python3  zwi_signature.py -h 
```

Here is a full example that shows how to sign, validate and remove a signature:

First, create a private (auth.pem) and public (auth.pub) key. Use these commands: 


```bash 
openssl genpkey -out auth.pem -algorithm rsa -pkeyopt rsa_keygen_bits:2048
openssl rsa -in auth.pem -out auth.pub -pubout
```

The first command creates the file "auth.pem". The second command makes the public key "auth.pub"  from the private key "auth.pem".
The private key "auth.pem" is used to sign the ZWI file.  

Here is an example showing how to sign a ZWI file called "wiki.zwi"

```bash
python3 zwi_signature.py -q -f -z wiki.zwi -i auth.pem -p KSF -a USA -k did:psqr:encycloreader.org/ksf/#publish
```

The command uses the option "-q" (no messages), and "-f" (full check of all files inside the ZWI file). To speed up the program, the option "-f" can be 
dropped. The above command adds the file "signature.json" inside the ZWI file "wiki.zwi".
Note that even when you use the private key  auth.pem, the public key auth.pub is also needed since it will be inserted inside signature.json.
The above example uses the DID:PSQR of the KSF with the public key of the KSF organization.

In order to validate the signature using a command-line, use these commands:

**1. Example:**  Using the public key from the ZWI file: 

```bash
python3 zwi_signature.py -f -v -z wiki.zwi
```

This is the fastest approach since the public key to verify the signature is taken from the ZWI file, i.e. from the "publicKey" field of the signature.json. 
If it was not included, then the algorithm will search for a
local file auth.pub. If the local file is not found, it will use the psqrKid field specified inside signature.json.  


**2. Example:**  The public key can also be taken from did:psqr. This requires the internet connection since the key will be taken from the remote server of the publisher:
 
```bash
python3 zwi_signature.py -f -v -z wiki.zwi -k did:psqr:encycloreader.org/ksf/#publish 
```

**3. Example:** The public key can be taken from the URL of the publisher as well. This requires the network connection and the URL to the publisher with
the stored public key. Here is an example:
 
```bash
python3 zwi_signature.py -f -v -z wiki.zwi -u https://encycloreader.org/ksf/
```

**4. Example:** Finally, here is an example when using the locally stored public key from the file "auth.pub":
 
```bash
python3 zwi_signature.py -f -v -z wiki.zwi -i auth.pub
```

This command reads the public key "auth.pub" and verifies it with the signature token inside "wiki.zwi". 

In all these cases the  argument "-f" ("full validation") checks  all files inside the ZWI file. 
Without "-f", the validation will be faster since no consistency checks of all 
files inside ZWI file is applied.

If the signature is correct, you should see:

```
== Signature is valid! ==
```

To remove the signature, run this command:

```bash
python3 zwi_signature.py -r -z wiki.zwi
```
You can find a  complete example in "examples/signature" directory. Run the file "README" to perform all these steps.

If you have a ZWI file with a signature, one can easy check it.  Extract the string from "Token": of the file "signature.json",
and parse it to the Web tool https://jwt.io/ . The right panel of this web page should decode it to something like this:

```json
{
  "alg": "RS256",
  "typ": "JWT"
}
{
  "authority": {
    "Organization": "Knowledge Standards Foundation (KSF)",
    "Time": "2022-08-19T17:33:16.382677",
    "Address": "info@encyclosphere.org, PO Box 31, Canal Winchester, OH 43110, USA",
    "PsqrKID": "DID PSQR"
  },
  "metadata": "d3131a613b6852ed1ac339163f3e52d7d43c68f8",
  "media": "91a8b762a18554b0729d1fa99f2f75029293959b"
}
```

(note the author and the hashes are arbitrary).
The ZWI viewer of the encycloreader.org automatically verifies the signature of the ZWI file. 
It checks the authenticity and integrity of the created ZWI. 
This signature tells that the ZWI file cannot be modified by the 3rd party 
without making the signature (ZWI) invalid.
As an example, click on the article title of https://encycloreader.org/db/ and look at the very end of each article. The green text
tells that the viewer has verified the token inside signature.json. 

##  A note about how the public key is served

The public key to verify the signature is taken from the psqrKid field specified inside signature.json. For the KSF, the location did:psqr:encycloreader.org/ksf/#publish corresponds to https://encycloreader.org/ksf/auth.pub. 

Note that the command:

```bash
python3 zwi_signature.py -f -v -z wiki.zwi
```

reads this public key file from ZWI (signature.json). The did:psqr:encycloreader.org/ksf/#publish is only used if publicKey is not set, or corrupted.

The KSF also surves the public key directly as https://encycloreader.org/ksf/ using the PHP script index.php: 

```bash
<?php
echo file_get_contents( "auth.pub" ); // get the contents
?>
```
This method to serve the public key requires more load on the system and is not used by default.
 

# Signing a ZWI file using jarsigner (not recommended) 

As any JAR/ZIP file, ZWI files can be signed by a publisher or issue using the standard Java approach. 
Here is a step-by-step instruction. In this example we show how to sign the ZWI file called `article.zwi` file with the example password `ksf2022`.
This certificate will be valid for 3650 days.

```bash
pass="ksf2022"
keytool -genkey -v -keystore ksf.ser -keypass $pass -storepass $pass \
   -dname "CN=KSF, OU=KSF, O=ENCYCLOSPHERE.ORG, L=OH, S=OH, C=US" \
   -alias ksf -keyalg RSA -validity 3650
```

You should answer the questions and this will create a file `ksf.ser` with your private key. It should be stored in a secure place. Then sign the article.zwi with this private key:

```bash
jarsigner -verbose -storepass $pass -keystore ksf.ser  article.zwi ksf
``` 

The last command adds a few files with the public key inside ZWI:

```
META-INF/MANIFEST.MF    
META-INF/KSF.SF         
META-INF/KSF.RSA    
```

Now everyone can verify that the file `article.zwi` has been signed. One can use either `jarsigner` or `keytool` commands:

```bash
keytool -printcert -jarfile article.zwi
```

This command prints the attributes of the signer:

```bash
Owner: CN=KSF, OU=KSF, O=ENCYCLOSPHERE.ORG, L=OH, ST=OH, C=US
Issuer: CN=KSF, OU=KSF, O=ENCYCLOSPHERE.ORG, L=OH, ST=OH, C=US
Serial number: 1249a59d
Valid from: Sat Jan 29 07:35:31 CST 2022 until: Jan 29 07:35:31 CST 2023
Certificate fingerprints:
	 MD5:  7B:FE:90:44:AE:81:18:98:70:C8:E9:EB:59:AA:57:41
	 SHA1: 60:E2:38:7C:A7:B1:1A:0D:61:AC:D2:08:8B:8B:09:5D:14:22:82:F7
	 SHA256: 1D:F6:AB:E9:CB:10:EF:3F:99:A3:52:EB:82:67:40:04:59:18:F7:61:93:87:A1:0D:AD:29:1D:3A:6F:D2:96:7E

```

Note that "Owner" and "Issuer" may not be the same. It should be mentioned that signing ZWI archive does not say anything about the license of the article inside the ZWI file. This is because a ZWI file contains verious data (CSS, JSON files, images) which may not be directly associated with the author of article, and thus do not fall under the license of the author of the article.

You can also verify it as this:

```bash
jarsigner -verify -certs article.zwi
```


To find the license of the article, run this command:

```bash
unzip -p article.zwi  metadata.json | grep License
```
The actual publisher of the article in the HTML, TXT, or wikicode format  (not the publisher of ZWI archive!) can be found as:

```bash
unzip -p article.zwi  metadata.json | grep Publisher
```

# ZWI index files

All ZWI files in the network are organized in the tree-like structure. This structure is described by the index files.
The main (global) index file in the root directory (typically inside ZWI/en) is called "index.csv.gz". It has the following structure: 

```bash
timestamp|total size in bytes|number of files|publisher/domain1 
timestamp|total size in bytes|number of files|publisher/domain2 
```
All entries are sorted according to the timestamp. The first line is the most recent update  from a given publisher.
Thus if you want to know when the directory has changed last time (and for which publisher), simple look at the first entry of this global index.
The last column points to the directory with the ZWI files, i.e  ZWI/en/publisher/domain1 etc. 

The directory "publisher" includes the 2nd level (or local) index files 
with the name "domain1.csv.gz" etc.  They describe the files located in ZWI/en/publisher/domain1. 
The entries in such files are organized as:

```bash
timestamp|filesize|name of the ZWI file without the extension .zwi
```

As for the global index,  the entries are organized the descending ordering according to the timestamp of files.
The most recent ZWI file goes first. 

Thus, if you want to know which file is the most recent in the network, look at the first entry 
in the global index "index.csv.gz". It will point to the 2nd level index file. Its first entry will
show the most recent ZWI file. 

If you have a collection of ZWI files, you can create the global and the local 
index files of this repository as:

```bash
pypy3 make_index.py -i /dir/.../ZWI/en 
```
The command creates all indexes in the network as described above. The second argument is the path
to the directory with the ZWI files. One can also use "python3", but it will be 40% slower. 

You can also update index of a list of specific publishers. Use this command to update "handwiki" and "citizendium",
but ignoring all other publishes. 

```bash
pypy3 make_index.py -i /data/encyclosphere/ZWI/en/ -p "handwiki,citizendium"
```

Note that this command reads the existing index files, and re-scan the directories for the 2 publishers given in the comma separated list. This will be much faster than recreating the index files of all publishers. 

One can also remove ZWI files on the disk, with the full update of all index files. Here is the example:

```bash
pypy3  make_index.py -i /data/encyclosphere/ZWI/en/ -p "handwiki" -d "wiki#Data,wiki#QuarkMeet"
```

This command removes two ZWI files with the name "wiki#Data" and "wiki#QuarkMeet" from the publisher "handwiki". Use the comma to separte the ZWI files. Note that the extension ".zwi" can be dropped. The files are not removed but moved to the "trash" directory. 
Note that this command is fast since it does not re-scan directories. 

# Useful commands

- `unzip -l Quark.zwi` - lists the content of the file (without unzip)
- `unzip -v -l Quark.zwi` - lists the content of the file (without unzip) and compression levels
- `unzip -p Quark.zwi metadata.json` - shows metadata
- `unzip -p Quark.zwi media.json` - shows included images
- `unzip -p Quark.zwi metadata.json > metadata.json` - extracts metadata
- `unzip -p Quark.zwi article.html > article.html` - extracts article only (no images)


S.Chekanov (KSF)


