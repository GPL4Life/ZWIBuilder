#!/usr/bin/env python
# -*- coding: utf-8 -*-

# You should run this script imidiatly after viewing online wiki since this tool actively uses cached images.
# It is recommended to run in withing a few minutes after watching an article in factseek.org 
# If you run this script after 1 day after viewing article, some cached data may not be available.
#
# @version 1.4, May 14, 2022 
# Changelog:
#   Re-use TCP connection for images
#
# @version 1.3, Decemeber 18, 2021 
# Changelog:
#   1 SourceURL was added
#   2 Added description and comment
# @version 1.1. October 30, 2021
# Changelog:
#   1 Version is numeric number
#   2 Pretty printing for JSON 

# S.V.Chekanov (KSF)
# 

import html2text
import requests
import re,os
import gzip 
import shutil # to save it locally
import zipfile
import json, urllib.parse
import tempfile

from bs4 import *
from time import time 
from hashlib import sha1
from os import path
from bs4 import BeautifulSoup
from short_description import *
#urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# hashing algo
hashing_algo = sha1

ZWI_VERSION = 1.3 # Version of ZWI files

# ISO Language Codes https://www.w3schools.com/tags/ref_language_codes.asp
Lang="en"

# find categories
PCategories = re.compile(r'\[\[Category:(.*?)\]\]')

def makeHash(data: str | bytes, encoding='utf-8') -> str:

    # encode if needed
    if isinstance(data, str):
        data = data.encode(encoding)

    return hashing_algo(
        data
    ).hexdigest()

# Get file hash
def hashFile(xfile: str) -> str:
    BLOCKSIZE = 65536

    if path.exists(xfile): 
        with open(xfile, 'rb') as afile:
            buf = afile.read(BLOCKSIZE)
            while len(buf) > 0:
                hashing_algo.update(buf)
                buf = afile.read(BLOCKSIZE)

        return hashing_algo.hexdigest()

    return ""

dirpath = tempfile.mkdtemp()

script_dir = os.path.dirname(os.path.realpath(__file__))
print(f"Current location= {script_dir}")

stime = str(int(time()))
#print("Time=",stime)
#print("TMP dir=",dirpath)

# this is where the data goes
img_dir = "data/media/images"
css_dir = "data/css"
folder_images = f"{dirpath}/{img_dir}"
folder_css = f"{dirpath}/{css_dir}"

ainput = output = ""
atitle = asource = ""
averbose = False

# get title from heading if possible
def getHeading(soup, stitle):

    ATIT = soup.find_all('h1')
    mtitle = stitle
    utitle = ""
    for t in ATIT:
        utitle = t.text.strip()
        break

    if utitle: 
        if len(utitle) >2:
            mtitle = utitle.strip()
            print(f" -> Extracted title from heading= {mtitle}")

    return mtitle

# get URL of the article
# apa : correct basename of the file
# publisher : publisher
def getURL(apa: str, publisher: str):
    url = ""

    SITE_URL = {
        "wikipedia": "https://en.wikipedia.org/wiki/",
        "wikitia": "https://wikitia.com/wiki/",
        "handwiki": "https://handwiki.org/wiki/",
        "citizendium": "https://en.citizendium.org/wiki/",
        "edutechwiki": "https://edutechwiki.unige.ch/en/",
        "ballotpedia": "https://ballotpedia.org/",
        "scholarpedia": "http://www.scholarpedia.org/article/",
        "encyclopediaofmath": "https://encyclopediaofmath.org/wiki/",
        "sep": "https://plato.stanford.edu/entries/"
    }.get(publisher)

    if not SITE_URL:
        SITE_URL = "https://en.wikipedia.org/wiki/"

    url = SITE_URL+apa

    return url

# CREATE FOLDER
def folder_create(images: list[str]) -> None:

    os.system(f"rm -rf {folder_images}")
    os.system(f"rm -rf {folder_css}")

    try:
        # folder creation
        os.system(f"mkdir -p {folder_images}")
        os.system(f"mkdir -p {folder_css}")
 
    # if folder exists with that name, ask another name
    except:
        print("Folder Exist with that name!")
        pass

    # image downloading start
    download_images(
        images, 
        folder_images
    )
 
# map to keep replacements for images 
imageReplacer = {}
cssReplacer = {}

def zipdir(
    path: str, 
    ziph: zipfile.ZipFile
    ) -> None:

    global aoutput, atitle, asource

    # ziph is zipfile handle
    for root, _, files in os.walk(path):
        for file in files:
            fpath = os.path.join(root, file)

            ziph.write(fpath,
                os.path.relpath(
                    fpath,
                    os.path.join(path, '..')
                )
            )

# DOWNLOAD ALL IMAGES FROM THAT URL
def download_images(images: list, folder_name: str):
    
    # intitial count is zero
    count = 0
    images = list(set(images))

    print(f"Total {len(images)} Images Found!")

    MaxImages2download = 2000
    if len(images) > MaxImages2download: # image abuse.. Skip 
        print(f"Found more than {MaxImages2download} images. No download for this abuse!") 
        return 0

    elif len(images) == 0: # no images found
        return 0

    allImages = []
    for i in range(len(images)):
        #print(images[i])
        #print("TYPE=",type(images[i]))
        # first try

        try:
            if images[i].get('src'):
                allImages.append(images[i]['src'])

        except IndexError:
            pass

        # second try
        try:
            if images[i].get('srcset'): 
                ss = images[i]['srcset'].split()
                for i in range(len(ss)):
                    allImages.append(ss[i])

        except IndexError:
            pass

        # 3rd try
        try:
            if images[i].get('data-srcset'): 
                ss = images[i]['data-srcset'].split()
                for i in range(len(ss)): 
                    allImages.append(ss[i])

        except IndexError:
            pass

        # 4th try
        try:
            if images[i].get('data-src'): 
                allImages.append(images[i]['data-src'])

        except IndexError:
            pass

    tcpSession = requests.Session()
    tcpSession.headers = {
        "accept": (
            "text/html,"
            "application/xhtml+xml,"
            "application/xml;q=0.9,"
            "image/avif,"
            "image/webp,"
            "image/apng,"
            "*/*;q=0.8,"
            "application/signed-exchange;v=b3;q=0.9"
        ),

        "accept-encoding": "gzip, deflate",
        "accept-language": "en-GB,en-US;q=0.9,en;q=0.8",
        "cache-control": "max-age=0",
        "sec-fetch-dest": "document",
        "sec-fetch-mode": "navigate",
        "sec-fetch-site": "none",
        "sec-fetch-user": "?1",
        "sec-gpc": "1",
        "dnt": "1",
        "upgrade-insecure-requests": "1",
        "user-agent": (
            "Mozilla/5.0 "
            "(Windows NT 10.0; Win64; x64) "
            "AppleWebKit/537.36 "
            "(KHTML, like Gecko) "
            "Chrome/100.0.4896.79 "
            "Safari/537.36"
        )
    }

    for i in range(len(allImages)):
        image_link = allImages[i]
        if image_link.find("//") == -1:
            continue

        if averbose: 
            # 1.data-srcset
            # 2.data-src
            # 3.data-fallback-src
            # 4.src
            # 5.srcset 

            print(f"{i} {allImages[i]}")

        #newname=os.path.basename(image_link)
        newname = image_link.split("/")[-1]
        if len(newname) < 2: 
            continue # too short name

        filename = f"{folder_name}/{newname}"
        if averbose:
            print(f"{count+1}) downloading= {image_link} to {filename}")

        # correct link when starts with //
        xurl = image_link
        if xurl.startswith("//"): 
            xurl = f"https:{image_link}"

        try:
            # r = requests.get(xurl, stream = True, timeout=(10, 300))
            r = tcpSession.get(xurl) # re-used TCP connection
        except requests.exceptions.RequestException as e:  # This is the correct syntax
            print(e) 
            continue 

        # check  svg used in for formulars. Wikipedia does not have file extension! 
        # formulars are made in SVG. The browser should know this by extension. 

        xnames = f"{img_dir}/{newname}"
        if newname.find(".") == -1:
            if image_link.find("/svg/") > -1: 
                filename = f"{folder_name}/{newname}.svg"
                xnames = f"{img_dir}/{newname}.svg"

        # remember replacements
        imageReplacer[image_link] = xnames

        # Check if the image was retrieved successfully
        if r.status_code == 200:
            with open(filename, "wb") as xfile:
                xfile.write(r.content)
            
            count += 1
            if averbose:
                print(f"Image sucessfully Downloaded= {filename}")

        else:
            print(f"{image_link} couldn\'t be retrieved")
            pass

    tcpSession.close()
    print(f"Downloaded= {count} images")        
    return count

# extract CSS
def extractCSS(
    soup: BeautifulSoup
    ) -> int:

    count = 0

    tcpSession = requests.Session()
    tcpSession.headers = {
        "accept": (
            "text/html,"
            "application/xhtml+xml,"
            "application/xml;q=0.9,"
            "image/avif,"
            "image/webp,"
            "image/apng,"
            "*/*;q=0.8,"
            "application/signed-exchange;v=b3;q=0.9"
        ),

        "accept-encoding": "gzip, deflate",
        "accept-language": "en-GB,en-US;q=0.9,en;q=0.8",
        "cache-control": "max-age=0",
        "sec-fetch-dest": "document",
        "sec-fetch-mode": "navigate",
        "sec-fetch-site": "none",
        "sec-fetch-user": "?1",
        "sec-gpc": "1",
        "dnt": "1",
        "upgrade-insecure-requests": "1",
        "user-agent": (
            "Mozilla/5.0 "
            "(Windows NT 10.0; Win64; x64) "
            "AppleWebKit/537.36 "
            "(KHTML, like Gecko) "
            "Chrome/100.0.4896.79 "
            "Safari/537.36"
        )
    }

    for link in soup('link'):
        if link.get('href'):
            if link.get('type') == 'text/css' \
            or link['href'].lower().endswith('.css') \
            or 'stylesheet' in link.get('rel'):

                new_type = 'text/css' if not link.get('type') else link['type']

                css = soup.new_tag('style', type=new_type)
                css['data-href'] = link['href']

                for attr in link.attrs:
                    if attr in ['href']:
                        continue

                    css[attr] = link[attr]
                    r_url = link['href']
                    if averbose:
                        print(css[attr], r_url) 

                    try:
                        r = tcpSession.get(
                            r_url, 
                            stream=True, 
                            timeout=(10, 300)
                        )

                        #r = requests.get(r_url, allow_redirects=True, timeout=(5, 200))
                    except requests.ConnectionError:
                       print(f" -> Connection error: {r_url}")
                       continue

                    except requests.ReadTimeout:
                       print(f" -> Timed out: {r_url}")
                       continue

                    newname = r_url.split("/")[-1]
                    filename = f"{folder_css}/{newname}"
                    cssReplacer[r_url] = f"{css_dir}/{newname}"
                    
                    count += 1
                    with open(filename,'w') as f:
                        f.write(r.text)
           
    print(f"Downloaded= {count} css files")
    return count 

# MAIN FUNCTION START
def main(
    html: str,
    wikitext: str
    ) -> None:

    global aoutput, atitle, dirpath, asource
 
    # content of URL
    #r = requests.get(url)
 
    # remove disambiguation
    if (html.find("Help:Disambiguation") > -1 and html.find("articles associated") > -1): 
        print("Detected Disambiguation -> Skip")
        return 

    if (html.find("Redirect to:") > -1 and len(html) < 1000):
        print("Redirect -> Skip")
        return

    if atitle.find("Category:") > -1 \
        or atitle.find("User:") > -1 \
        or atitle.find("Help:") > -1:
        return 

    # make txt file too
    parser = html2text.HTML2Text()
    parser.ignore_links = True
    parser.ignore_images = True
    parser.ignore_emphasis = True
    parser.wrap_links = True
    parser.body_width = 0

    articletxt = parser.handle(html)
    xlines = articletxt.splitlines()
    non_empty_lines = ""

    for line in xlines:
        line = line.strip()

        if len(line) < 3:
            line = "\n"

        if line == "|":
            line=" "

        line = line.replace("---", "\n")
        non_empty_lines += f"{line}\n"
        # print(len(line)," ",line)

    articletxt = non_empty_lines
    articletxt = re.sub(r'\n\s*\n', '\n\n', articletxt) # replace 2 new lines 
    articletxt = articletxt.strip()

    if len(articletxt) < 300:
        print(f"Article has length= {len(articletxt)} -> too short. Exit!")
        return

    if len(articletxt) < 500:
        if articletxt.find("no text in this page") > -1:
            return

    if len(articletxt) < 500:
        if articletxt.find("redirect") > -1: 
            return    

    # Parse HTML Code
    soup = BeautifulSoup(html, 'html.parser')

    # internal title. Sometime ZWI file name does not correspond to actual title
    # internal_title=soup.h1;

    # nicely looking
    # html = soup.prettify()   #prettify the html

    # find all images in URL
    images = soup.findAll('img')
  
    # Call folder create function
    folder_create(images)

    # extract CSS
    extractCSS(soup)

    xmedia = {} 
    htmlnew = html

    print("-> Make CSS replacements")
    n = 0
    for key, cssfile in cssReplacer.items():
        if path.exists( f"{dirpath}/{cssfile}" ):
            xmedia[cssfile] = hashFile( f"{dirpath}/{cssfile}" ) 

            if averbose:
                print(f"{n}) {key} replaced by {cssfile}")

            htmlnew = htmlnew.replace(key, cssfile)
            n += 1

    print("-> Make image replacements")
    n = 0
    for key, img in imageReplacer.items():
        if path.exists( f"{dirpath}/{img}" ):

            xmedia[img] = hashFile(f"{dirpath}/{img}")

            if averbose:
                print(f"{n}) {key} replaced by {img}")

            htmlnew = htmlnew.replace(key, img)
            n += 1

    # now remove lock file and write correct ZWI file
    if os.path.exists(aoutput):
        os.remove( aoutput )

    z = zipfile.ZipFile(
        aoutput, 
        'w', 
        compression=zipfile.ZIP_DEFLATED
    )  # this is a zip archive

    z.writestr("article.html", htmlnew)
    z.writestr("article.wikitext", wikitext)
    z.writestr("article.txt", articletxt)

    ncategories = []
    resCat = PCategories.findall(wikitext)
    print(f"Nr of categories found= {len(resCat)}")

    if len(resCat) > 0:
        for cat in resCat:

            catText = (
                cat
                .replace("\n","")
                .replace("_"," ")
                .strip()
            )

            strpBar=cat.split("|",1)

            if len(strpBar) > 1:
                catText = strpBar[0].strip()

            ncategories.append(catText)

    #for key in  imageReplacer:
    #    z.write(imageReplacer[key].encode(), imageReplacer[key].encode(), zipfile.ZIP_DEFLATED )
    zipdir(f"{dirpath}/data/", z)
    #htmltit=htmltit.replace(".html.gz","")
    #htmltit=htmltit.replace(".html","")

    primary = "article.html"

    # create hashes
    content={
        "article.html": makeHash(htmlnew),
        "article.wikitext": makeHash(wikitext),
        "article.txt": makeHash(articletxt)
    }

    # perhaps make the license
    # customizable?
    license="CC BY-SA 3.0"
    topics = []
    revisions = []
    rating = []
    generator = "MediaWiki"
    comment = ""

    # topics are set for HandWiki only
    if asource == "handwiki":
        tt = atitle.split(":", 1)
        if len(tt) > 1:
            topics.append(tt[0])

    elif asource == "sep":
        license="The Stanford Encyclopedia of Philosophy (SEP) license"

        # SEP is very special about file names
        # we need to extract it from H1 
        primary = "article.html"
        atitle = getHeading(soup,atitle)

        comment = "Not for redistribution"
        generator = "Custom CMS"

    if len(wikitext) > 5: 
        primary = "article.wikitext"

    # get URL
    xpa = os.path.basename(aoutput)
    xpa = xpa.replace(".zwi", "")
    xpa = urllib.parse.unquote_plus(xpa)
    url = getURL(xpa, asource)

    description= getShortDescription(
        asource,
        wikitext,
        htmlnew,
        articletxt
    )

    print(f"Short description= {description}")

    metadata = {
        "ZWIversion": ZWI_VERSION, 
        "Title": atitle,
        "Lang": Lang,
        "Content": content, 
        "Primary": primary, 
        "Revisions": revisions,
        "Publisher": asource,
        "CreatorNames": [],
        "ContributorNames": [],
        "LastModified": stime,
        "TimeCreated": stime,
        "Categories": ncategories,
        "Topics": topics,
        "Description": description,
        "GeneratorName": generator,
        "Comment": comment,
        "Rating": rating,
        "License": license,
        "SourceURL": url
    }

    z.writestr(
        "metadata.json", 
        json.dumps(
            metadata,
            indent=4
        )
    )

    z.writestr(
        "media.json", 
        json.dumps(
            xmedia,
            indent=4, 
            sort_keys=True
        )
    )

    z.close()

    shutil.rmtree(dirpath)   
    print(f"Cleared= {dirpath}")     

    print(f"Created= {aoutput}")
 

# Main procesor
def makeZWI(
    html_file, 
    zwi_file, 
    ztitle, 
    zsource
    ) -> None:
    global aoutput, atitle, asource

    ainput = html_file
    aoutput = zwi_file
    atitle = ztitle
    asource = zsource

    print(f"Input= {ainput}")
    print(f"Output= {aoutput}")
    print(f"Article title= {atitle}")
    print(f"Encyclopedia source= {asource}")
    print(f"Is verbose= {averbose}")

    # create progress file. 
    # It is useful to avoid locking when another program runs
    if os.path.exists(aoutput):
        b = os.path.getsize( aoutput )
        if b < 50:
            print(f"Locking file= {aoutput} exists! Exit!")
            return

    else:
        with open(aoutput, "w") as progress_file:
            progress_file.write("In progress")

    HTML = ""
    index = ainput
    try:

        ret = None
        if index.endswith('.html'):
            ret = open(
                index, 
                'r', 
                encoding='utf-8'
            )

        elif index.endswith('.html.gz'):
            ret =  gzip.open(
                index, 
                'rt',
                encoding='utf-8'
            )

        if not ret:
            print('No index file! Exit!')
            return

        # read
        data = str(ret.read())
        
        # close file descriptor
        ret.close()

        # prepare file  header and footer
        with open(f"{script_dir}/html_header.html", encoding="utf-8") as fd:
            data_head = fd.read()

        with open(f"{script_dir}/html_footer.html", encoding="utf-8")as fd:
            data_footer = fd.read()

        HTML = data_head + data + data_footer
        HTML = urllib.parse.unquote(HTML) # replace %28 and %29 with ()

    except IOError:
        print("Error with HTML creation")

    # get wikitext
    wikitextfile = index.replace(".html.gz", ".wikitext.gz")
    wikitext = "None"

    if os.path.exists(wikitextfile):
        with gzip.open(wikitextfile, 'rt',encoding='utf-8') as gzfd:
            wikitext = gzfd.read()

    # CALL MAIN FUNCTION
    main(HTML, str(wikitext))